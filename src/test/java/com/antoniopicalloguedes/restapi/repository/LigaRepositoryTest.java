package com.antoniopicalloguedes.restapi.repository;


import com.antoniopicalloguedes.restapi.model.EquipoFutbol;
import com.antoniopicalloguedes.restapi.model.LigaFutbol;
import com.antoniopicalloguedes.restapi.service.EquipoFutbolService;
import com.antoniopicalloguedes.restapi.service.LigaFutbolService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LigaFutbol.class)
public class LigaRepositoryTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LigaFutbolService ligaFutbolService;


    LigaFutbol ligaFutbol1;

    LigaFutbol ligaFutbol2;


    List<LigaFutbol> ligaList= new ArrayList<>();

    @BeforeEach
    void setUp() {
        ligaFutbol1 = new LigaFutbol(1,"LaLiga","España");
        ligaFutbol2 = new LigaFutbol(6,"Serie A","Italia");
        ligaList.add(ligaFutbol1);
        ligaList.add(ligaFutbol2);
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void getLiga() throws Exception {
        when(ligaFutbolService.getLigaFutbol(1)).thenReturn(ligaFutbol1);
        when(ligaFutbolService.getLigaFutbol(2)).thenReturn(ligaFutbol2);
        this.mockMvc.perform(get("/ligaFutbol/" + "1")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void getAllLigas() throws  Exception {
        when(ligaFutbolService.getAllLigasFutbol()).thenReturn(ligaList);
        this.mockMvc.perform(get("/ligasFutbol"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void createLiga() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(ligaFutbol1);

        when(ligaFutbolService.createLigaFutbol(ligaFutbol1)).thenReturn(any());
        this.mockMvc.perform(post("/ligaFutbol")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void updateLiga() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(ligaFutbol1);

        when(ligaFutbolService.updateLigaFutbol(ligaFutbol1))
                .thenReturn(any());
        this.mockMvc.perform(put("/ligaFutbol")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void deleteLiga() throws Exception {
        when(ligaFutbolService.deleteLigaFutbol(1))
                .thenReturn("Liga borrada correctamente");
        this.mockMvc.perform(delete("/ligaFutbol/" + "1"))
                .andDo(print()).andExpect(status().isOk());

    }




}
