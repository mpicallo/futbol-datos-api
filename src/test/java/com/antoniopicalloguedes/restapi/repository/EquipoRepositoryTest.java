package com.antoniopicalloguedes.restapi.repository;




import com.antoniopicalloguedes.restapi.model.EquipoFutbol;

import com.antoniopicalloguedes.restapi.service.EquipoFutbolService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EquipoFutbol.class)
public class EquipoRepositoryTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EquipoFutbolService equipoFutbolService;


    EquipoFutbol equipoFutbol1;

    EquipoFutbol equipoFutbol2;


    List<EquipoFutbol> equipoList= new ArrayList<>();

    @BeforeEach
    void setUp() {
        equipoFutbol1 = new EquipoFutbol(1,"Real Madrid CF",2,"LaLiga");
        equipoFutbol2 = new EquipoFutbol(2,"FC Barcelona",1,"LaLiga");
        equipoList.add(equipoFutbol1);
        equipoList.add(equipoFutbol2);
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void getEquipo() throws Exception {
        when(equipoFutbolService.getEquipoFutbol(1)).thenReturn(equipoFutbol1);
        when(equipoFutbolService.getEquipoFutbol(2)).thenReturn(equipoFutbol2);
        this.mockMvc.perform(get("/equipoFutbol/" + "1")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void getAllEquipos() throws  Exception {
        when(equipoFutbolService.getAllEquiposFutbol()).thenReturn(equipoList);
        this.mockMvc.perform(get("/equiposFutbol"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void createEquipo() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(equipoFutbol1);

        when(equipoFutbolService.createEquipoFutbol(equipoFutbol1)).thenReturn(any());
        this.mockMvc.perform(post("/equipoFutbol")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void updateEquipo() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(equipoFutbol1);

        when(equipoFutbolService.updateEquipoFutbol(equipoFutbol1))
                .thenReturn(any());
        this.mockMvc.perform(put("/equipoFutbol")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void deleteEquipo() throws Exception {
        when(equipoFutbolService.deleteEquipoFutbol(1))
                .thenReturn("Equipo borrado correctamente");
        this.mockMvc.perform(delete("/equipoFutbol/" + "1"))
                .andDo(print()).andExpect(status().isOk());

    }




}
