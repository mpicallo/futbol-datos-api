package com.antoniopicalloguedes.restapi.repository;

import com.antoniopicalloguedes.restapi.model.JugadorFutbol;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
public class JugadorRepositoryTest {

  //  given - when - then

    @Autowired
    private JugadorFutbolRepository jugadorRepository;
    JugadorFutbol jugador;

    @BeforeEach
    void setUp() {
        jugador = new JugadorFutbol(1,"jjj",2,"1",new Date(),"España",50000000,"Real Madrid CF");
        jugadorRepository.save(jugador);
    }

    @AfterEach
    void tearDown() {
        jugador = null;
        jugadorRepository.deleteAll();
    }

    // Test case SUCCESS

    @Test
    void testFindByJugador()
    {
        List<JugadorFutbol> list = jugadorRepository.findByNombreContaining("jjj");
        assertThat(list.get(0).getId()).isEqualTo(jugador.getId());
        assertThat(list.get(0).getEquipo())
                .isEqualTo(jugador.getEquipo());
    }

    // Test case FAILURE
    @Test
    void testJugadorFutbol_NotFound()
    {
        List<JugadorFutbol> list = jugadorRepository.findByNombreContaining("jjj");
        assertThat(list.isEmpty()).isTrue();
    }
}
