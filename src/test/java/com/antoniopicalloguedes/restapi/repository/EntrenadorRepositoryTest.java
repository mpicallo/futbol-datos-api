package com.antoniopicalloguedes.restapi.repository;

import com.antoniopicalloguedes.restapi.model.EntrenadorFutbol;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
public class EntrenadorRepositoryTest {


    @Autowired
    private EntrenadorFutbolRepository entrenadorFutbolRepository;

    EntrenadorFutbol entrenador;

    @BeforeEach
    void setUp() {
        entrenador = new EntrenadorFutbol(2,"Xavi",new Date(),"España","FC Barcelona");
        entrenadorFutbolRepository.save(entrenador);
    }

    @AfterEach
    void tearDown() {
        entrenador = null;
        entrenadorFutbolRepository.deleteAll();
    }

    // Test case SUCCESS

    @Test
    void testFindByEntrenador()
    {
        List<EntrenadorFutbol> list = entrenadorFutbolRepository.findByNombreContaining("Xavi");
        assertThat(list.get(0).getId()).isEqualTo(entrenador.getId());
        assertThat(list.get(0).getEquipo())
                .isEqualTo(entrenador.getEquipo());
    }

    // Test case FAILURE
    @Test
    void testEntrenadorFutbol_NotFound()
    {
        List<EntrenadorFutbol> list = entrenadorFutbolRepository.findByNombreContaining("rrr");
        assertThat(list.isEmpty()).isTrue();
    }











}
