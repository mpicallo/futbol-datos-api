package com.antoniopicalloguedes.restapi.service.impl;

import com.antoniopicalloguedes.restapi.model.EntrenadorFutbol;
import com.antoniopicalloguedes.restapi.model.JugadorFutbol;
import com.antoniopicalloguedes.restapi.repository.EntrenadorFutbolRepository;
import com.antoniopicalloguedes.restapi.repository.JugadorFutbolRepository;
import com.antoniopicalloguedes.restapi.service.EntrenadorFutbolService;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class EntrenadorServiceImplTest {


    @Mock
    private EntrenadorFutbolRepository entrenadorFutbolRepository;
    private EntrenadorFutbolService entrenadorFutbolService;
    AutoCloseable autoCloseable;
    EntrenadorFutbol entrenador;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        entrenadorFutbolService = new EntrenadorFutbolServiceImpl(entrenadorFutbolRepository);
        entrenador = new EntrenadorFutbol(2,"Xavi",new Date(),"España","FC Barcelona");
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void testCreateEntrenador() {
        mock(EntrenadorFutbol.class);
        mock(EntrenadorFutbolRepository.class);

        when(entrenadorFutbolRepository.save(entrenador)).thenReturn(entrenador);
        assertThat(entrenadorFutbolService.createEntrenadorFutbol(entrenador)).isEqualTo("Success");

    }

    @Test
    void testUpdateEntrenador() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(entrenadorFutbolRepository.save(entrenador)).thenReturn(entrenador);
        assertThat(entrenadorFutbolService.updateEntrenadorFutbol(entrenador)).isEqualTo("Success");
    }

    @Test
    void testDeleteEntrenador() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class, Mockito.CALLS_REAL_METHODS);

        doAnswer(Answers.CALLS_REAL_METHODS).when(entrenadorFutbolRepository)
                .deleteById(any());
        assertThat(entrenadorFutbolService.deleteEntrenadorFutbol(1)).isEqualTo("Success");
    }

    @Test
    void testGetEntrenador() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(entrenadorFutbolRepository.findById(1)).thenReturn(Optional.ofNullable(entrenador));

        assertThat(entrenadorFutbolService.getEntrenadorFutbol(1).getNombre())
                .isEqualTo(entrenador.getNombre());
    }

    @Test
    void testGetByName() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(entrenadorFutbolRepository.findByNombreContaining("Xavi")).
                thenReturn(new ArrayList<EntrenadorFutbol>(Collections.singleton(entrenador)));

        assertThat(entrenadorFutbolService.getByEntrenadorFutbolNombre("Xavi").get(0).getId()).
                isEqualTo(entrenador.getId());
    }

    @Test
    void testGetAllEntrenadores() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(entrenadorFutbolRepository.findAll()).thenReturn(new ArrayList<EntrenadorFutbol>(
                Collections.singleton(entrenador)
        ));

        assertThat(entrenadorFutbolService.getAllEntrenadoresFutbol().get(0).getNombre()).
                isEqualTo(entrenador.getNombre());

    }


}
