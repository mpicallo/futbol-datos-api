package com.antoniopicalloguedes.restapi.service.impl;


import com.antoniopicalloguedes.restapi.model.EquipoFutbol;
import com.antoniopicalloguedes.restapi.model.JugadorFutbol;
import com.antoniopicalloguedes.restapi.repository.EquipoFutbolRepository;
import com.antoniopicalloguedes.restapi.repository.JugadorFutbolRepository;
import com.antoniopicalloguedes.restapi.service.EquipoFutbolService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class EquipoServiceImplTest {

    @Mock
    private EquipoFutbolRepository equipoFutbolRepository;
    private EquipoFutbolService equipoFutbolService;
    AutoCloseable autoCloseable;
    EquipoFutbol equipoFutbol;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        equipoFutbolService = new EquipoFutbolServiceImpl(equipoFutbolRepository);
        equipoFutbol = new EquipoFutbol(1,"Real Madrid CF",2,"LaLiga");
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void testCreateEquipo() {
        mock(EquipoFutbol.class);
        mock(EquipoFutbolRepository.class);

        when(equipoFutbolRepository.save(equipoFutbol)).thenReturn(equipoFutbol);
        assertThat(equipoFutbolService.createEquipoFutbol(equipoFutbol)).isEqualTo("Success");

    }

    @Test
    void testUpdateEquipo() {
        mock(EquipoFutbol.class);
        mock(EquipoFutbolRepository.class);

        when(equipoFutbolRepository.save(equipoFutbol)).thenReturn(equipoFutbol);
        assertThat(equipoFutbolService.createEquipoFutbol(equipoFutbol)).isEqualTo("Success");
    }

    @Test
    void testDeleteEquipo() {
        mock(EquipoFutbol.class);
        mock(EquipoFutbolRepository.class, Mockito.CALLS_REAL_METHODS);

        doAnswer(Answers.CALLS_REAL_METHODS).when(equipoFutbolRepository)
                .deleteById(any());
        assertThat(equipoFutbolService.deleteEquipoFutbol(1)).isEqualTo("Success");
    }

    @Test
    void testGetEquipo() {
        mock(EquipoFutbol.class);
        mock(EquipoFutbolRepository.class);

        when(equipoFutbolRepository.findById(1)).thenReturn(Optional.ofNullable(equipoFutbol));

        assertThat(equipoFutbolService.getEquipoFutbol(1).getNombre())
                .isEqualTo(equipoFutbol.getNombre());
    }

    @Test
    void testGetByName() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(equipoFutbolRepository.findByNombreContaining("Real Madrid CF")).
                thenReturn(new ArrayList<EquipoFutbol>(Collections.singleton(equipoFutbol)));

        assertThat(equipoFutbolService.getByEquipoFutbolNombre("Real Madrid CF").get(0).getId()).
                isEqualTo(equipoFutbol.getId());
    }

    @Test
    void testGetAllEquipos() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(equipoFutbolRepository.findAll()).thenReturn(new ArrayList<EquipoFutbol>(
                Collections.singleton(equipoFutbol)
        ));

        assertThat(equipoFutbolService.getAllEquiposFutbol().get(0).getNombre()).
                isEqualTo(equipoFutbol.getNombre());

    }



}
