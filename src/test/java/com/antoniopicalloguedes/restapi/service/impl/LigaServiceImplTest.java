package com.antoniopicalloguedes.restapi.service.impl;

import com.antoniopicalloguedes.restapi.model.EquipoFutbol;
import com.antoniopicalloguedes.restapi.model.JugadorFutbol;
import com.antoniopicalloguedes.restapi.model.LigaFutbol;
import com.antoniopicalloguedes.restapi.repository.EquipoFutbolRepository;
import com.antoniopicalloguedes.restapi.repository.JugadorFutbolRepository;
import com.antoniopicalloguedes.restapi.repository.LigaFutbolRepository;
import com.antoniopicalloguedes.restapi.service.EquipoFutbolService;
import com.antoniopicalloguedes.restapi.service.LigaFutbolService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class LigaServiceImplTest {

    @Mock
    private LigaFutbolRepository ligaFutbolRepository;
    private LigaFutbolService ligaFutbolService;
    AutoCloseable autoCloseable;
    LigaFutbol ligaFutbol;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        ligaFutbolService = new LigaFutbolServiceImpl(ligaFutbolRepository);
        ligaFutbol = new LigaFutbol(1,"LaLiga","España");
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void testCreateLiga() {
        mock(LigaFutbol.class);
        mock(LigaFutbolRepository.class);

        when(ligaFutbolRepository.save(ligaFutbol)).thenReturn(ligaFutbol);
        assertThat(ligaFutbolService.createLigaFutbol(ligaFutbol)).isEqualTo("Success");

    }

    @Test
    void testUpdateLiga() {
        mock(LigaFutbol.class);
        mock(LigaFutbolRepository.class);

        when(ligaFutbolRepository.save(ligaFutbol)).thenReturn(ligaFutbol);
        assertThat(ligaFutbolService.createLigaFutbol(ligaFutbol)).isEqualTo("Success");
    }

    @Test
    void testDeleteLiga() {
        mock(LigaFutbol.class);
        mock(LigaFutbolRepository.class, Mockito.CALLS_REAL_METHODS);

        doAnswer(Answers.CALLS_REAL_METHODS).when(ligaFutbolRepository)
                .deleteById(any());
        assertThat(ligaFutbolService.deleteLigaFutbol(1)).isEqualTo("Success");
    }

    @Test
    void testGetLiga(){
        mock(LigaFutbol.class);
        mock(LigaFutbolRepository.class);

        when(ligaFutbolRepository.findById(1)).thenReturn(Optional.ofNullable(ligaFutbol));

        assertThat(ligaFutbolService.getLigaFutbol(1).getNombre())
                .isEqualTo(ligaFutbol.getNombre());
    }

    @Test
    void testGetByName() {
        mock(LigaFutbol.class);
        mock(LigaFutbolRepository.class);

        when(ligaFutbolRepository.findByNombreContaining("LaLiga")).
                thenReturn(new ArrayList<LigaFutbol>(Collections.singleton(ligaFutbol)));

        assertThat(ligaFutbolService.getByLigaFutbolNombre("LaLiga").get(0).getId()).
                isEqualTo(ligaFutbol.getId());
    }

    @Test
    void testGetAllLigas() {
        mock(LigaFutbol.class);
        mock(LigaFutbolRepository.class);

        when(ligaFutbolRepository.findAll()).thenReturn(new ArrayList<LigaFutbol>(
                Collections.singleton(ligaFutbol)
        ));

        assertThat(ligaFutbolService.getAllLigasFutbol().get(0).getNombre()).
                isEqualTo(ligaFutbol.getNombre());

    }

}
