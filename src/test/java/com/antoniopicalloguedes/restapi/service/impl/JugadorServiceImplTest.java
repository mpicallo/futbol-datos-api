package com.antoniopicalloguedes.restapi.service.impl;

import com.antoniopicalloguedes.restapi.model.JugadorFutbol;
import com.antoniopicalloguedes.restapi.repository.JugadorFutbolRepository;
import com.antoniopicalloguedes.restapi.service.JugadorFutbolService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class JugadorServiceImplTest {

    @Mock
    private JugadorFutbolRepository jugadorRepository;
    private JugadorFutbolService jugadorService;
    AutoCloseable autoCloseable;
    JugadorFutbol jugador;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        jugadorService = new JugadorFutbolServiceImpl(jugadorRepository);
        jugador = new JugadorFutbol(1,"jjj",2,"1",new Date(),"España",50000000,"Real Madrid CF");
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void testCreateJugador() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(jugadorRepository.save(jugador)).thenReturn(jugador);
        assertThat(jugadorService.createJugadorFutbol(jugador)).isEqualTo("Success");

    }

    @Test
    void testUpdateJugador() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(jugadorRepository.save(jugador)).thenReturn(jugador);
        assertThat(jugadorService.updateJugadorFutbol(jugador)).isEqualTo("Success");
    }

    @Test
    void testDeleteJugador() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class, Mockito.CALLS_REAL_METHODS);

        doAnswer(Answers.CALLS_REAL_METHODS).when(jugadorRepository)
                .deleteById(any());
        assertThat(jugadorService.deleteJugadorFutbol(1)).isEqualTo("Success");
    }

    @Test
    void testGetJugador() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(jugadorRepository.findById(1)).thenReturn(Optional.ofNullable(jugador));

        assertThat(jugadorService.getJugadorFutbol(1).getNombre())
                .isEqualTo(jugador.getNombre());
    }

    @Test
    void testGetByName() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(jugadorRepository.findByNombreContaining("jjj")).
                thenReturn(new ArrayList<JugadorFutbol>(Collections.singleton(jugador)));

        assertThat(jugadorService.getByJugadorFutbolNombre("jjj").get(0).getId()).
                isEqualTo(jugador.getId());
    }

    @Test
    void testGetAllJugadores() {
        mock(JugadorFutbol.class);
        mock(JugadorFutbolRepository.class);

        when(jugadorRepository.findAll()).thenReturn(new ArrayList<JugadorFutbol>(
                Collections.singleton(jugador)
        ));

        assertThat(jugadorService.getAllJugadoresFutbol().get(0).getNumero()).
                isEqualTo(jugador.getNumero());

    }


}