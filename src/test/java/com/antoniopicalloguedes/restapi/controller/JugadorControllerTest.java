package com.antoniopicalloguedes.restapi.controller;

import com.antoniopicalloguedes.restapi.model.JugadorFutbol;
import com.antoniopicalloguedes.restapi.service.JugadorFutbolService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(JugadorFutbolController.class)
class JugadorControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private JugadorFutbolService jugadorService;
    JugadorFutbol jugadorOne;
    JugadorFutbol jugadorTwo;
    List<JugadorFutbol> jugadorList= new ArrayList<>();

    @BeforeEach
    void setUp() {
        jugadorOne = new JugadorFutbol(1,"jjj",2,"1",new Date(),"España",50000000,"Real Madrid CF");
        jugadorTwo = new JugadorFutbol(2,"jjj",2,"1",new Date(),"España",50000000,"Real Madrid CF");
        jugadorList.add(jugadorOne);
        jugadorList.add(jugadorTwo);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getJugador() throws Exception {
        when(jugadorService.getJugadorFutbol(1)).thenReturn(jugadorOne);
        when(jugadorService.getJugadorFutbol(2)).thenReturn(jugadorTwo);
        this.mockMvc.perform(get("/jugadorFutbol/" + "1")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void getAllJugadores() throws  Exception {
        when(jugadorService.getAllJugadoresFutbol()).thenReturn(jugadorList);
        this.mockMvc.perform(get("/jugadoresFutbol"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void createJugador() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jugadorOne);

        when(jugadorService.createJugadorFutbol(jugadorOne)).thenReturn(any());
        this.mockMvc.perform(post("/jugadorFutbol")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void updateJugador() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jugadorOne);

        when(jugadorService.updateJugadorFutbol(jugadorOne))
                .thenReturn(any());
        this.mockMvc.perform(put("/jugadorFutbol")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void deleteJugador() throws Exception {
        when(jugadorService.deleteJugadorFutbol(1))
                .thenReturn("Jugador borrado correctamente");
        this.mockMvc.perform(delete("/jugadorFutbol/" + "1"))
                .andDo(print()).andExpect(status().isOk());

    }
}
