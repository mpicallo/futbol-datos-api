package com.antoniopicalloguedes.restapi.controller;


import com.antoniopicalloguedes.restapi.model.EntrenadorFutbol;
import com.antoniopicalloguedes.restapi.service.EntrenadorFutbolService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EntrenadorFutbolController.class)
public class EntrenadorControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EntrenadorFutbolService entrenadorFutbolService;
    EntrenadorFutbol entrenadorFutbol1;
    EntrenadorFutbol entrenadorFutbol2;
    List<EntrenadorFutbol> entrenadorList= new ArrayList<>();

    @BeforeEach
    void setUp() {
        entrenadorFutbol1 = new EntrenadorFutbol(1,"Carlo Ancelloty",new Date(),"Italia","Real Madrid CF");
        entrenadorFutbol2 = new EntrenadorFutbol(2,"Xavi",new Date(),"España","FC Barcelona");
        entrenadorList.add(entrenadorFutbol1);
        entrenadorList.add(entrenadorFutbol2);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getENtrenador() throws Exception {
        when(entrenadorFutbolService.getEntrenadorFutbol(1)).thenReturn(entrenadorFutbol1);
        when(entrenadorFutbolService.getEntrenadorFutbol(2)).thenReturn(entrenadorFutbol2);
        this.mockMvc.perform(get("/entrenadorFutbol/" + "1")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void getAllEntrenadores() throws  Exception {
        when(entrenadorFutbolService.getAllEntrenadoresFutbol()).thenReturn(entrenadorList);
        this.mockMvc.perform(get("/entrenadoresFutbol"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void createEntrenador() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(entrenadorFutbol1);

        when(entrenadorFutbolService.createEntrenadorFutbol(entrenadorFutbol1)).thenReturn(any());
        this.mockMvc.perform(post("/entrenadorFutbol")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void updateEntrenador() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(entrenadorFutbol1);

        when(entrenadorFutbolService.updateEntrenadorFutbol(entrenadorFutbol1))
                .thenReturn(any());
        this.mockMvc.perform(put("/entrenadorFutbol")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void deleteEntrenador() throws Exception {
        when(entrenadorFutbolService.deleteEntrenadorFutbol(1))
                .thenReturn("Entrenador borrado correctamente");
        this.mockMvc.perform(delete("/entrenadorFutbol/" + "1"))
                .andDo(print()).andExpect(status().isOk());

    }








}
