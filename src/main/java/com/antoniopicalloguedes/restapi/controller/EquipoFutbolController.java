package com.antoniopicalloguedes.restapi.controller;



import com.antoniopicalloguedes.restapi.exception.MessageException;
import com.antoniopicalloguedes.restapi.exception.ResourceNotFoundException;
import com.antoniopicalloguedes.restapi.model.EquipoFutbol;
import com.antoniopicalloguedes.restapi.service.EquipoFutbolService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "EquiposFutbol", description = "API de Equipos futbol")
@RestController
public class EquipoFutbolController {


    EquipoFutbolService equipoFutbolService;

    public EquipoFutbolController(EquipoFutbolService equipoFutbolService) {
        this.equipoFutbolService = equipoFutbolService;
    }


    @GetMapping("equipoFutbolId/{equipoFutbolId}")
    @Operation(
            summary = "Devuelve un equipo de futbol por su Id",
            description = "Devuelve un equipo de futbol por su Id",
            tags = { "EquiposFutbol" })
    @ApiOperation(value = "equipo futbol por Id")
    public ResponseEntity<Object> getEquipoFutbol(@PathVariable("equipoFutbolId") Integer equipoFutbolId)
    {
        if(equipoFutbolService.getEquipoFutbol(equipoFutbolId).equals(null)){
            throw new ResourceNotFoundException("NO ENCONTRADO EQUIPO "+equipoFutbolId);
        }else{
            return new ResponseEntity<>(equipoFutbolService.getEquipoFutbol(equipoFutbolId), HttpStatus.OK);
        }
    }


    @GetMapping("equipoFutbolNombre/{equipoFutbolNombre}")
    @Operation(
            summary = "Devuelve un equipo por su Nombre",
            description = "Devuelve un equipo por su Nombre",
            tags = { "EquiposFutbol" })
    @ApiOperation(value = "Equipo futbol por su nombre")
    public ResponseEntity<List<EquipoFutbol>> getEquipoFutbolByNombre(@PathVariable("equipoFutbolNombre") String equipoFutbolNombre)
    {
        if(equipoFutbolService.getByEquipoFutbolNombre(equipoFutbolNombre).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(equipoFutbolService.getByEquipoFutbolNombre(equipoFutbolNombre), HttpStatus.OK);
        }
    }


    @GetMapping("equiposFutbol/")
    @Operation(
            summary = "Devuelve todos los equipos futbol",
            description = "Devuelve todos los equipos futbol",
            tags = { "EquiposFutbol" })
    @ApiOperation(value = "Todos los Equipos futbol")
    public ResponseEntity<List<EquipoFutbol>> getAllEquiposFutbol()
    {
        if(equipoFutbolService.getAllEquiposFutbol().isEmpty()){
            throw new MessageException("LISTA VACIA");
        }else{
            return new ResponseEntity<>(equipoFutbolService.getAllEquiposFutbol(), HttpStatus.OK);
        }
    }


    @PostMapping("equipoFutbol/")
    @Operation(
            summary = "Crea un equipo de futbol",
            description = "Crea un equipo de futbol",
            tags = { "EquiposFutbol" })
    @ApiOperation(value = "Crea un Equipo de futbol")
    public ResponseEntity<EquipoFutbol> createEquipoFutbol(@RequestBody EquipoFutbol equipoFutbol)
    {
        return new ResponseEntity<>(equipoFutbolService.createEquipoFutbol(equipoFutbol), HttpStatus.CREATED);
    }


    @PutMapping("equipoFutbol/")
    @Operation(
            summary = "Actualiza un equipo de futbol",
            description = "Actualiza un equipo de futbol",
            tags = { "EquiposFutbol" })
    @ApiOperation(value = "Actualiza datos de un Equipo de futbol")
    public ResponseEntity<EquipoFutbol> updateEquipoFutbol(@RequestBody EquipoFutbol equipoFutbol)
    {
        equipoFutbolService.updateEquipoFutbol(equipoFutbol);
        return new ResponseEntity<>(equipoFutbolService.updateEquipoFutbol(equipoFutbol), HttpStatus.OK);
    }

    @DeleteMapping("equipoFutbol/{equipoFutbolId}")
    @Operation(
            summary = "Elimina un equipo de futbol por su Id",
            description = "Elimina un equipo de futbol",
            tags = { "EquiposFutbol" })
    @ApiOperation(value = "Elimina un equipo de futbol")
    public ResponseEntity<String> deleteEquipoFutbol(@PathVariable("equipoFutbolId") Integer equipoId)
    {
        return new ResponseEntity<>(equipoFutbolService.deleteEquipoFutbol(equipoId), HttpStatus.OK);
    }



    @GetMapping("equiposFutbolCount/")
    @Operation(
            summary = "Devuelve Cantidad equipos futbol",
            description = "Devuelve Cantidad equipos futbol",
            tags = { "EquiposFutbol" })
    @ApiOperation(value = "Cantidad de equipos futbol")
    public ResponseEntity<Long> getCountEquiposFutbol()

    {
        return new ResponseEntity<>(equipoFutbolService.countEquiposFutbol(), HttpStatus.OK);

    }
}
