package com.antoniopicalloguedes.restapi.controller;



import com.antoniopicalloguedes.restapi.exception.MessageException;
import com.antoniopicalloguedes.restapi.exception.ResourceNotFoundException;
import com.antoniopicalloguedes.restapi.model.JugadorFutbol;
import com.antoniopicalloguedes.restapi.service.JugadorFutbolService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "JugadoresFutbol", description = "API de Jugadores futbol")
@RestController
public class JugadorFutbolController {


    JugadorFutbolService jugadorFutbolService;

    public JugadorFutbolController(JugadorFutbolService jugadorFutbolService) {
        this.jugadorFutbolService = jugadorFutbolService;
    }


    @GetMapping("jugadorFutbolId/{jugadorFutbolId}")
    @Operation(
            summary = "Devuelve un jugador de futbol por su Id Id",
            description = "Devuelve un jugador de futbol por su Id Id",
            tags = { "JugadoresFutbol" })
    @ApiOperation(value = "Jugador futbol por Id")
    public ResponseEntity<Object> getJugadorFutbol(@PathVariable("jugadorFutbolId") Integer jugadorFutbolId)
    {
        if(jugadorFutbolService.getJugadorFutbol(jugadorFutbolId).equals(null)){
            throw new ResourceNotFoundException("NO ENCONTRADO JUGADOR "+jugadorFutbolId);
        }else{
            return new ResponseEntity<>(jugadorFutbolService.getJugadorFutbol(jugadorFutbolId), HttpStatus.OK);
        }
    }


    @GetMapping("jugadoresFutbolPais/{pais}")
    @Operation(
            summary = "Devuelve jugadores de futbol por pais",
            description = "Devuelve jugadores de futbol por pais",
            tags = { "JugadoresFutbol" })
    @ApiOperation(value = "Jugadores de futbol por pais")
    public ResponseEntity<List<JugadorFutbol>> getJugadoresFutbolByPais(@PathVariable("pais") String pais)
    {
        if(jugadorFutbolService.getByJugadorFutbolPais(pais).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(jugadorFutbolService.getByJugadorFutbolPais(pais), HttpStatus.OK);
        }
    }



    @GetMapping("jugadorFutbolNombre/{jugadorFutbolNombre}")
    @Operation(
            summary = "Devuelve un jugador por su Nombre",
            description = "Devuelve un jugador por su Nombre",
            tags = { "JugadoresFutbol" })
    @ApiOperation(value = "Jugador futbol por su nombre")
    public ResponseEntity<List<JugadorFutbol>> getJugadorFutbolByNombre(@PathVariable("jugadorFutbolNombre") String jugadorFutbolNombre)
    {
        if(jugadorFutbolService.getByJugadorFutbolNombre(jugadorFutbolNombre).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(jugadorFutbolService.getByJugadorFutbolNombre(jugadorFutbolNombre), HttpStatus.OK);
        }
    }


    @GetMapping("jugadoresFutbol/")
    @Operation(
            summary = "Devuelve todos los jugadores futbol",
            description = "Devuelve todos los jugadores futbol",
            tags = { "JugadoresFutbol" })
    @ApiOperation(value = "Todos los jugadores futbol")
    public ResponseEntity<List<JugadorFutbol>> getAllJugadoresFutbol()
    {
        if(jugadorFutbolService.getAllJugadoresFutbol().isEmpty()){
            throw new MessageException("LISTA VACIA");
        }else{
            return new ResponseEntity<>(jugadorFutbolService.getAllJugadoresFutbol(), HttpStatus.OK);
        }
    }


    @PostMapping("jugadorFutbol/")
    @Operation(
            summary = "Crea un jugador de futbol",
            description = "Crea un jugador de futbol",
            tags = { "JugadoresFutbol" })
    @ApiOperation(value = "Crea un Jugador de futbol")
    public ResponseEntity<JugadorFutbol> createJugadorFutbol(@RequestBody JugadorFutbol jugadorFutbol)
    {
        return new ResponseEntity<>(jugadorFutbolService.createJugadorFutbol(jugadorFutbol), HttpStatus.CREATED);
    }


    @PutMapping("jugadorFutbol/")
    @Operation(
            summary = "Actualiza un jugador de futbol",
            description = "Actualiza un jugador de futbol",
            tags = { "JugadoresFutbol" })
    @ApiOperation(value = "Actualiza datos de un Jugador de futbol")
    public ResponseEntity<JugadorFutbol> updateJugadorFutbol(@RequestBody JugadorFutbol jugadorFutbol)
    {
        jugadorFutbolService.updateJugadorFutbol(jugadorFutbol);
        return new ResponseEntity<>(jugadorFutbolService.updateJugadorFutbol(jugadorFutbol), HttpStatus.OK);
    }

    @DeleteMapping("jugadorFutbol/{jugadorFutbolId}")
    @Operation(
            summary = "Elimina un jugador de futbol por su Id",
            description = "Elimina un jugador de futbol",
            tags = { "JugadoresFutbol" })
    @ApiOperation(value = "Elimina un jugador de futbol")
    public ResponseEntity<String> deleteJugadorFutbol(@PathVariable("jugadorFutbolId") Integer jugadorId)
    {
        return new ResponseEntity<>(jugadorFutbolService.deleteJugadorFutbol(jugadorId), HttpStatus.OK);
    }



    @GetMapping("jugadoresFutbolCount/")
    @Operation(
            summary = "Devuelve Cantidad jugadores futbol",
            description = "Devuelve Cantidad jugadores futbol",
            tags = { "JugadoresFutbol" })
    @ApiOperation(value = "Cantidad de jugadores futbol")
    public ResponseEntity<Long> getCountJugadoresFutbol()

    {
        return new ResponseEntity<>(jugadorFutbolService.countJugadoresFutbol(), HttpStatus.OK);

    }

}
