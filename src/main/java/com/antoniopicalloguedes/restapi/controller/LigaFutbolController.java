package com.antoniopicalloguedes.restapi.controller;



import com.antoniopicalloguedes.restapi.exception.MessageException;
import com.antoniopicalloguedes.restapi.exception.ResourceNotFoundException;
import com.antoniopicalloguedes.restapi.model.LigaFutbol;
import com.antoniopicalloguedes.restapi.service.LigaFutbolService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "LigasFutbol", description = "API de Ligas futbol")
@RestController
public class LigaFutbolController {


    LigaFutbolService ligaFutbolService;

    public LigaFutbolController(LigaFutbolService ligaFutbolService) {
        this.ligaFutbolService = ligaFutbolService;
    }


    @GetMapping("ligaFutbolId/{ligaFutbolId}")
    @Operation(
            summary = "Devuelve una liga de futbol por su Id",
            description = "Devuelve una liga de futbol por su Id",
            tags = { "LigasFutbol" })
    @ApiOperation(value = "liga futbol por Id")
    public ResponseEntity<Object> getLigaFutbol(@PathVariable("ligaFutbolId") Integer ligaFutbolId)
    {
        if(ligaFutbolService.getLigaFutbol(ligaFutbolId).equals(null)){
            throw new ResourceNotFoundException("NO ENCONTRADO LIGA "+ligaFutbolId);
        }else{
            return new ResponseEntity<>(ligaFutbolService.getLigaFutbol(ligaFutbolId), HttpStatus.OK);
        }
    }


    @GetMapping("ligasFutbolPais/{pais}")
    @Operation(
            summary = "Devuelve ligas de futbol por pais",
            description = "Devuelve ligas de futbol por pais",
            tags = { "LigasFutbol" })
    @ApiOperation(value = "Ligas de futbol por pais")
    public ResponseEntity<List<LigaFutbol>> getLigasFutbolByPais(@PathVariable("pais") String pais)
    {
        if(ligaFutbolService.getByLigaFutbolPais(pais).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(ligaFutbolService.getByLigaFutbolPais(pais), HttpStatus.OK);
        }
    }



    @GetMapping("ligaFutbolNombre/{ligaFutbolNombre}")
    @Operation(
            summary = "Devuelve una liga por su Nombre",
            description = "Devuelve una liga por su Nombre",
            tags = { "LigasFutbol" })
    @ApiOperation(value = "Liga futbol por su nombre")
    public ResponseEntity<List<LigaFutbol>> getLigaFutbolByNombre(@PathVariable("ligaFutbolNombre") String ligaFutbolNombre)
    {
        if(ligaFutbolService.getByLigaFutbolNombre(ligaFutbolNombre).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(ligaFutbolService.getByLigaFutbolNombre(ligaFutbolNombre), HttpStatus.OK);
        }
    }


    @GetMapping("ligasFutbol/")
    @Operation(
            summary = "Devuelve todas las ligas futbol",
            description = "Devuelve todas las ligas futbol",
            tags = { "LigasFutbol" })
    @ApiOperation(value = "Todas las Ligas futbol")
    public ResponseEntity<List<LigaFutbol>> getAllLigasFutbol()
    {
        if(ligaFutbolService.getAllLigasFutbol().isEmpty()){
            throw new MessageException("LISTA VACIA");
        }else{
            return new ResponseEntity<>(ligaFutbolService.getAllLigasFutbol(), HttpStatus.OK);
        }
    }


    @PostMapping("ligaFutbol/")
    @Operation(
            summary = "Crea una liga de futbol",
            description = "Crea una liga de futbol",
            tags = { "LigasFutbol" })
    @ApiOperation(value = "Crea una liga de futbol")
    public ResponseEntity<LigaFutbol> createLigaFutbol(@RequestBody LigaFutbol ligaFutbol)
    {
        return new ResponseEntity<>(ligaFutbolService.createLigaFutbol(ligaFutbol), HttpStatus.CREATED);
    }


    @PutMapping("ligaFutbol/")
    @Operation(
            summary = "Actualiza una liga de futbol",
            description = "Actualiza una liga de futbol",
            tags = { "LigasFutbol" })
    @ApiOperation(value = "Actualiza datos de una Liga de futbol")
    public ResponseEntity<LigaFutbol> updateLigaFutbol(@RequestBody LigaFutbol ligaFutbol)
    {
        ligaFutbolService.updateLigaFutbol(ligaFutbol);
        return new ResponseEntity<>(ligaFutbolService.updateLigaFutbol(ligaFutbol), HttpStatus.OK);
    }

    @DeleteMapping("ligaFutbol/{ligaFutbolId}")
    @Operation(
            summary = "Elimina una liga de futbol por su Id",
            description = "Elimina una liga de futbol",
            tags = { "LigasFutbol" })
    @ApiOperation(value = "Elimina una liga de futbol")
    public ResponseEntity<String> deleteLigaFutbol(@PathVariable("ligaFutbolId") Integer ligaId)
    {
        return new ResponseEntity<>(ligaFutbolService.deleteLigaFutbol(ligaId), HttpStatus.OK);
    }



    @GetMapping("ligasFutbolCount/")
    @Operation(
            summary = "Devuelve Cantidad ligas futbol",
            description = "Devuelve Cantidad ligas futbol",
            tags = { "LigasFutbol" })
    @ApiOperation(value = "Cantidad de ligas futbol")
    public ResponseEntity<Long> getCountLigasFutbol()

    {
        return new ResponseEntity<>(ligaFutbolService.countLigasFutbol(), HttpStatus.OK);

    }
}
