package com.antoniopicalloguedes.restapi.controller;



import com.antoniopicalloguedes.restapi.exception.MessageException;
import com.antoniopicalloguedes.restapi.exception.ResourceNotFoundException;
import com.antoniopicalloguedes.restapi.model.EntrenadorFutbol;
import com.antoniopicalloguedes.restapi.service.EntrenadorFutbolService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "EntrenadoresFutbol", description = "API de Entrenadores futbol")
@RestController
public class EntrenadorFutbolController {


    EntrenadorFutbolService entrenadorFutbolService;

    public EntrenadorFutbolController(EntrenadorFutbolService entrenadorFutbolService) {
        this.entrenadorFutbolService = entrenadorFutbolService;
    }


    @GetMapping("entrenadorFutbolId/{entrenadorFutbolId}")
    @Operation(
            summary = "Devuelve un entrenador de futbol por su Id",
            description = "Devuelve un entrenador de futbol por su Id",
            tags = { "EntrenadoresFutbol" })
    @ApiOperation(value = "entrenador futbol por Id")
    public ResponseEntity<Object> getEntrenadorFutbol(@PathVariable("entrenadorFutbolId") Integer entrenadorFutbolId)
    {
        if(entrenadorFutbolService.getEntrenadorFutbol(entrenadorFutbolId).equals(null)){
            throw new ResourceNotFoundException("NO ENCONTRADO ENTRENADOR "+entrenadorFutbolId);
        }else{
            return new ResponseEntity<>(entrenadorFutbolService.getEntrenadorFutbol(entrenadorFutbolId), HttpStatus.OK);
        }
    }


    @GetMapping("entrenadoresFutbolPais/{pais}")
    @Operation(
            summary = "Devuelve entrenadores de futbol por pais",
            description = "Devuelve entrenadores de futbol por pais",
            tags = { "EntrenadoresFutbol" })
    @ApiOperation(value = "Entrenadores de futbol por pais")
    public ResponseEntity<List<EntrenadorFutbol>> getEntrenadorsFutbolByPais(@PathVariable("pais") String pais)
    {
        if(entrenadorFutbolService.getByEntrenadorFutbolPais(pais).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(entrenadorFutbolService.getByEntrenadorFutbolPais(pais), HttpStatus.OK);
        }
    }



    @GetMapping("entrenadorFutbolNombre/{entrenadorFutbolNombre}")
    @Operation(
            summary = "Devuelve un entrenador por su Nombre",
            description = "Devuelve un entrenador por su Nombre",
            tags = { "EntrenadoresFutbol" })
    @ApiOperation(value = "Entrenador futbol por su nombre")
    public ResponseEntity<List<EntrenadorFutbol>> getEntrenadorFutbolByNombre(@PathVariable("entrenadorFutbolNombre") String entrenadorFutbolNombre)
    {
        if(entrenadorFutbolService.getByEntrenadorFutbolNombre(entrenadorFutbolNombre).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(entrenadorFutbolService.getByEntrenadorFutbolNombre(entrenadorFutbolNombre), HttpStatus.OK);
        }
    }


    @GetMapping("entrenadoresFutbol/")
    @Operation(
            summary = "Devuelve todos los entrenadores futbol",
            description = "Devuelve todos los entrenadores futbol",
            tags = { "EntrenadoresFutbol" })
    @ApiOperation(value = "Todos los Entrenadores futbol")
    public ResponseEntity<List<EntrenadorFutbol>> getAllEntrenadorsFutbol()
    {
        if(entrenadorFutbolService.getAllEntrenadoresFutbol().isEmpty()){
            throw new MessageException("LISTA VACIA");
        }else{
            return new ResponseEntity<>(entrenadorFutbolService.getAllEntrenadoresFutbol(), HttpStatus.OK);
        }
    }


    @PostMapping("entrenadorFutbol/")
    @Operation(
            summary = "Crea un entrenador de futbol",
            description = "Crea un entrenador de futbol",
            tags = { "EntrenadoresFutbol" })
    @ApiOperation(value = "Crea un Entrenador de futbol")
    public ResponseEntity<EntrenadorFutbol> createEntrenadorFutbol(@RequestBody EntrenadorFutbol entrenadorFutbol)
    {
        return new ResponseEntity<>(entrenadorFutbolService.createEntrenadorFutbol(entrenadorFutbol), HttpStatus.CREATED);
    }


    @PutMapping("entrenadorFutbol/")
    @Operation(
            summary = "Actualiza un entrenador de futbol",
            description = "Actualiza un entrenador de futbol",
            tags = { "EntrenadoresFutbol" })
    @ApiOperation(value = "Actualiza datos de un Entrenador de futbol")
    public ResponseEntity<EntrenadorFutbol> updateEntrenadorFutbol(@RequestBody EntrenadorFutbol entrenadorFutbol)
    {
        entrenadorFutbolService.updateEntrenadorFutbol(entrenadorFutbol);
        return new ResponseEntity<>(entrenadorFutbolService.updateEntrenadorFutbol(entrenadorFutbol), HttpStatus.OK);
    }

    @DeleteMapping("entrenadorFutbol/{entrenadorFutbolId}")
    @Operation(
            summary = "Elimina un entrenador de futbol por su Id",
            description = "Elimina un entrenador de futbol",
            tags = { "EntrenadoresFutbol" })
    @ApiOperation(value = "Elimina un entrenador de futbol")
    public ResponseEntity<String> deleteEntrenadorFutbol(@PathVariable("entrenadorFutbolId") Integer entrenadorId)
    {
        return new ResponseEntity<>(entrenadorFutbolService.deleteEntrenadorFutbol(entrenadorId), HttpStatus.OK);
    }



    @GetMapping("entrenadoresFutbolCount/")
    @Operation(
            summary = "Devuelve Cantidad entrenadores futbol",
            description = "Devuelve Cantidad entrenadores futbol",
            tags = { "EntrenadoresFutbol" })
    @ApiOperation(value = "Cantidad de entrenadores futbol")
    public ResponseEntity<Long> getCountEntrenadoresFutbol()

    {
        return new ResponseEntity<>(entrenadorFutbolService.countEntrenadoresFutbol(), HttpStatus.OK);

    }
}
