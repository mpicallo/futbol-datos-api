package com.antoniopicalloguedes.restapi.service;

import com.antoniopicalloguedes.restapi.model.JugadorFutbol;

import java.util.List;

public interface JugadorFutbolService {

    public JugadorFutbol createJugadorFutbol(JugadorFutbol jugadorFutbol);

    public JugadorFutbol updateJugadorFutbol(JugadorFutbol jugadorFutbol);

    public String deleteJugadorFutbol(Integer jugadorFutbolId);

    public JugadorFutbol getJugadorFutbol(Integer jugadorFutbolId);

    public List<JugadorFutbol> getAllJugadoresFutbol();

    public List<JugadorFutbol> getByJugadorFutbolNombre(String jugadoFutbolrNombre);

    public List<JugadorFutbol> getByJugadorFutbolPais(String pais);

    public Long countJugadoresFutbol();

}
