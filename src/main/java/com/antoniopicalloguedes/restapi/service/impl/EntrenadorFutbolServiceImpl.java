package com.antoniopicalloguedes.restapi.service.impl;

import com.antoniopicalloguedes.restapi.exception.ResourceNotFoundException;
import com.antoniopicalloguedes.restapi.model.EntrenadorFutbol;
import com.antoniopicalloguedes.restapi.repository.EntrenadorFutbolRepository;
import com.antoniopicalloguedes.restapi.service.EntrenadorFutbolService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EntrenadorFutbolServiceImpl implements EntrenadorFutbolService {

    EntrenadorFutbolRepository entrenadorFutbolRepository;

    public EntrenadorFutbolServiceImpl(EntrenadorFutbolRepository entrenadorFutbolRepository) {
        this.entrenadorFutbolRepository = entrenadorFutbolRepository;
    }


    @Override
    public EntrenadorFutbol createEntrenadorFutbol(EntrenadorFutbol entrenadorFutbol) {
        entrenadorFutbolRepository.save(entrenadorFutbol);
        return entrenadorFutbol;
    }

    @Override
    public EntrenadorFutbol updateEntrenadorFutbol(EntrenadorFutbol entrenadorFutbol) {
        entrenadorFutbolRepository.save(entrenadorFutbol);
        return entrenadorFutbol;
    }

    @Override
    public String deleteEntrenadorFutbol(Integer entrenadorFutbolId) {
        entrenadorFutbolRepository.deleteById(entrenadorFutbolId);
        return "Success";
    }

    @Override
    public EntrenadorFutbol getEntrenadorFutbol(Integer entrenadorFutbolId) {
        if(entrenadorFutbolRepository.findById(entrenadorFutbolId).isEmpty())
            throw new ResourceNotFoundException("Entrenador solicitado no existe");
        return entrenadorFutbolRepository.findById(entrenadorFutbolId).get();
    }

    @Override
    public List<EntrenadorFutbol> getAllEntrenadoresFutbol() {
        return entrenadorFutbolRepository.findAll();
    }

    @Override
    public List<EntrenadorFutbol> getByEntrenadorFutbolNombre(String entrenadorFutbolrNombre) {
        return entrenadorFutbolRepository.findByNombreContaining(entrenadorFutbolrNombre);
    }

    @Override
    public List<EntrenadorFutbol> getByEntrenadorFutbolPais(String pais) {
        return entrenadorFutbolRepository.findByPaisContaining(pais);
    }

    @Override
    public Long countEntrenadoresFutbol() {
        return entrenadorFutbolRepository.count();
    }

}
