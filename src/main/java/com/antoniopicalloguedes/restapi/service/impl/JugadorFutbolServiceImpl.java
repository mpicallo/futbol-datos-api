package com.antoniopicalloguedes.restapi.service.impl;

import com.antoniopicalloguedes.restapi.exception.MessageException;
import com.antoniopicalloguedes.restapi.model.JugadorFutbol;

import com.antoniopicalloguedes.restapi.repository.JugadorFutbolRepository;
import com.antoniopicalloguedes.restapi.service.JugadorFutbolService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class JugadorFutbolServiceImpl implements JugadorFutbolService {

    JugadorFutbolRepository jugadorFutbolRepository;

    public JugadorFutbolServiceImpl(JugadorFutbolRepository jugadorFutbolRepository) {
        this.jugadorFutbolRepository = jugadorFutbolRepository;
    }


    @Override
    public JugadorFutbol createJugadorFutbol(JugadorFutbol jugadorFutbol) {
        jugadorFutbolRepository.save(jugadorFutbol);
        return jugadorFutbol;
    }

    @Override
    public JugadorFutbol updateJugadorFutbol(JugadorFutbol jugadorFutbol) {
         jugadorFutbolRepository.save(jugadorFutbol);
        return jugadorFutbol;
    }

    @Override
    public String deleteJugadorFutbol(Integer jugadorFutbolId) {
        jugadorFutbolRepository.deleteById(jugadorFutbolId);
        return "Eliminado";
    }

    @Override
    public JugadorFutbol getJugadorFutbol(Integer jugadorFutbolId) {
        if(jugadorFutbolRepository.findById(jugadorFutbolId).isEmpty())
            throw new MessageException("Jugador solicitado no existe");
        return jugadorFutbolRepository.findById(jugadorFutbolId).get();
    }

    @Override
    public List<JugadorFutbol> getAllJugadoresFutbol() {
        return jugadorFutbolRepository.findAll();
    }

    @Override
    public List<JugadorFutbol> getByJugadorFutbolNombre(String jugadoFutbolrNombre) {
        return jugadorFutbolRepository.findByNombreContaining(jugadoFutbolrNombre);
    }

    @Override
    public List<JugadorFutbol> getByJugadorFutbolPais(String pais) {
        return jugadorFutbolRepository.findByPaisContaining(pais);
    }

    @Override
    public Long countJugadoresFutbol() {
        return jugadorFutbolRepository.count();
    }


}
