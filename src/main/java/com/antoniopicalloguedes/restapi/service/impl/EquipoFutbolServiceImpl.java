package com.antoniopicalloguedes.restapi.service.impl;

import com.antoniopicalloguedes.restapi.exception.MessageException;
import com.antoniopicalloguedes.restapi.model.EquipoFutbol;
import com.antoniopicalloguedes.restapi.repository.EquipoFutbolRepository;
import com.antoniopicalloguedes.restapi.service.EquipoFutbolService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EquipoFutbolServiceImpl implements EquipoFutbolService {

    EquipoFutbolRepository equipoFutbolRepository;

    public EquipoFutbolServiceImpl(EquipoFutbolRepository equipoFutbolRepository) {
        this.equipoFutbolRepository = equipoFutbolRepository;
    }


    @Override
    public EquipoFutbol createEquipoFutbol(EquipoFutbol equipoFutbol) {
        equipoFutbolRepository.save(equipoFutbol);
        return equipoFutbol;
    }

    @Override
    public EquipoFutbol updateEquipoFutbol(EquipoFutbol equipoFutbol) {
        equipoFutbolRepository.save(equipoFutbol);
        return equipoFutbol;
    }

    @Override
    public String deleteEquipoFutbol(Integer equipoFutbolId) {
        equipoFutbolRepository.deleteById(equipoFutbolId);
        return "Success";
    }

    @Override
    public EquipoFutbol getEquipoFutbol(Integer equipoFutbolId) {
        if(equipoFutbolRepository.findById(equipoFutbolId).isEmpty())
            throw new MessageException("Equipo solicitado no existe");
        return equipoFutbolRepository.findById(equipoFutbolId).get();
    }

    @Override
    public List<EquipoFutbol> getAllEquiposFutbol() {
        return equipoFutbolRepository.findAll();
    }

    @Override
    public List<EquipoFutbol> getByEquipoFutbolNombre(String equipoFutbolrNombre) {
        return equipoFutbolRepository.findByNombreContaining(equipoFutbolrNombre);
    }

    @Override
    public Long countEquiposFutbol() {
        return equipoFutbolRepository.count();
    }

}
