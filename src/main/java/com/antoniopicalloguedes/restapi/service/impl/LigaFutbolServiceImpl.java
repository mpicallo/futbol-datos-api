package com.antoniopicalloguedes.restapi.service.impl;

import com.antoniopicalloguedes.restapi.exception.MessageException;
import com.antoniopicalloguedes.restapi.model.LigaFutbol;
import com.antoniopicalloguedes.restapi.repository.LigaFutbolRepository;
import com.antoniopicalloguedes.restapi.service.LigaFutbolService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class LigaFutbolServiceImpl implements LigaFutbolService {

    LigaFutbolRepository ligaFutbolRepository;

    public LigaFutbolServiceImpl(LigaFutbolRepository ligaFutbolRepository) {
        this.ligaFutbolRepository = ligaFutbolRepository;
    }


    @Override
    public LigaFutbol createLigaFutbol(LigaFutbol ligaFutbol) {
        ligaFutbolRepository.save(ligaFutbol);
        return ligaFutbol;
    }

    @Override
    public LigaFutbol updateLigaFutbol(LigaFutbol ligaFutbol) {
        ligaFutbolRepository.save(ligaFutbol);
        return ligaFutbol;
    }

    @Override
    public String deleteLigaFutbol(Integer ligaFutbolId) {
        ligaFutbolRepository.deleteById(ligaFutbolId);
        return "Success";
    }

    @Override
    public LigaFutbol getLigaFutbol(Integer ligaFutbolId) {
        if(ligaFutbolRepository.findById(ligaFutbolId).isEmpty())
            throw new MessageException("Liga solicitada no existe");
        return ligaFutbolRepository.findById(ligaFutbolId).get();
    }

    @Override
    public List<LigaFutbol> getAllLigasFutbol() {
        return ligaFutbolRepository.findAll();
    }

    @Override
    public List<LigaFutbol> getByLigaFutbolNombre(String ligaFutbolrNombre) {
        return ligaFutbolRepository.findByNombreContaining(ligaFutbolrNombre);
    }

    @Override
    public List<LigaFutbol> getByLigaFutbolPais(String pais) {
        return ligaFutbolRepository.findByPaisContaining(pais);
    }

    @Override
    public Long countLigasFutbol() {
        return ligaFutbolRepository.count();
    }

}
