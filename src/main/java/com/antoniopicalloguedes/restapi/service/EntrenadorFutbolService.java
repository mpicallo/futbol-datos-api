package com.antoniopicalloguedes.restapi.service;

import com.antoniopicalloguedes.restapi.model.EntrenadorFutbol;

import java.util.List;

public interface EntrenadorFutbolService {

    public EntrenadorFutbol createEntrenadorFutbol(EntrenadorFutbol entrenadorFutbol);

    public EntrenadorFutbol updateEntrenadorFutbol(EntrenadorFutbol entrenadorFutbol);

    public String deleteEntrenadorFutbol(Integer entrenadorFutbolId);

    public EntrenadorFutbol getEntrenadorFutbol(Integer entrenadorFutbolId);

    public List<EntrenadorFutbol> getAllEntrenadoresFutbol();

    public List<EntrenadorFutbol> getByEntrenadorFutbolNombre(String entrenadorFutbolrNombre);

    public List<EntrenadorFutbol> getByEntrenadorFutbolPais(String pais);

    public Long countEntrenadoresFutbol();

}
