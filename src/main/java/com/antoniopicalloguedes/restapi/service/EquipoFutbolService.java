package com.antoniopicalloguedes.restapi.service;

import com.antoniopicalloguedes.restapi.model.EquipoFutbol;

import java.util.List;

public interface EquipoFutbolService {

    public EquipoFutbol createEquipoFutbol(EquipoFutbol equipoFutbol);

    public EquipoFutbol updateEquipoFutbol(EquipoFutbol equipoFutbol);

    public String deleteEquipoFutbol(Integer equipoFutbolId);

    public EquipoFutbol getEquipoFutbol(Integer equipoFutbolId);

    public List<EquipoFutbol> getAllEquiposFutbol();

    public List<EquipoFutbol> getByEquipoFutbolNombre(String equipoFutbolrNombre);

    public Long countEquiposFutbol();

}
