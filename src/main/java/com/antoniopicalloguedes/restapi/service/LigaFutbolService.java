package com.antoniopicalloguedes.restapi.service;

import com.antoniopicalloguedes.restapi.model.LigaFutbol;

import java.util.List;

public interface LigaFutbolService {

    public LigaFutbol createLigaFutbol(LigaFutbol ligaFutbol);

    public LigaFutbol updateLigaFutbol(LigaFutbol ligaFutbol);

    public String deleteLigaFutbol(Integer ligaFutbolId);

    public LigaFutbol getLigaFutbol(Integer ligaFutbolId);

    public List<LigaFutbol> getAllLigasFutbol();

    public List<LigaFutbol> getByLigaFutbolNombre(String ligaFutbolrNombre);

    public List<LigaFutbol> getByLigaFutbolPais(String pais);

    public Long countLigasFutbol();

}
