package com.antoniopicalloguedes.restapi.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="jugador")
@ApiModel(description = "Muestra los datos de los jugadores")
public class JugadorFutbol {

    @Id
    @Column(name = "cod_jugador_PK")
    @ApiModelProperty(notes="Este es el id unico de cada jugador")
    private Integer Id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "numero")
    private Integer numero;

    @Column(name = "posicion")
    private String posicion;

    @Column(name = "fecha_nacim")
    private Date nacimiento;

    @Column(name = "pais")
    private String pais;

    @Column(name = "valor")
    private Integer valor;

    @Column(name = "equipo")
    private String equipo;


    public JugadorFutbol() {
    }

    public JugadorFutbol(Integer id, String nombre, Integer numero, String posicion, Date nacimiento, String pais, Integer valor, String equipo) {
        Id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.posicion = posicion;
        this.nacimiento = nacimiento;
        this.pais = pais;
        this.valor = valor;
        this.equipo = equipo;
    }


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }


}
