package com.antoniopicalloguedes.restapi.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="equipo")
@ApiModel(description = "Muestra los datos de los equipos")
public class EquipoFutbol {

    @Id
    @Column(name = "cod_equipo_PK")
    @ApiModelProperty(notes="Este es el id unico de cada equipo")
    private Integer Id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "puesto")
    private Integer puesto;

    @Column(name = "liga")
    private String liga;


    public EquipoFutbol() {
    }

    public EquipoFutbol(Integer id, String nombre, Integer puesto, String liga) {
        Id = id;
        this.nombre = nombre;
        this.puesto = puesto;
        this.liga = liga;
    }


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPuesto() {
        return puesto;
    }

    public void setPuesto(Integer puesto) {
        this.puesto = puesto;
    }

    public String getLiga() {
        return liga;
    }

    public void setPais(String liga) {
        this.liga = liga;
    }

}
