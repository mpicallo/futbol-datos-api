package com.antoniopicalloguedes.restapi.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="entrenador")
@ApiModel(description = "Muestra los datos los entrenadores registrados")
public class EntrenadorFutbol {

    @Id
    @Column(name = "cod_entrenador_PK")
    @ApiModelProperty(notes="Este es el id unico de cada entrenador")
    private Integer Id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "fecha_nacim")
    private Date nacimiento;

    @Column(name = "pais")
    private String pais;

    @Column(name = "equipo")
    private String equipo;


    public EntrenadorFutbol() {
    }

    public EntrenadorFutbol(Integer id, String nombre, Date nacimiento, String pais, String equipo) {
        Id = id;
        this.nombre = nombre;
        this.nacimiento = nacimiento;
        this.pais = pais;
        this.equipo = equipo;
    }


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

}
