package com.antoniopicalloguedes.restapi.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="liga")
@ApiModel(description = "Muestra los datos de las ligas registradas de varios paises")
public class LigaFutbol {

    @Id
    @Column(name = "cod_liga_PK")
    @ApiModelProperty(notes="Este es el id unico de cada liga")
    private Integer Id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "pais")
    private String pais;


    public LigaFutbol() {
    }

    public LigaFutbol(Integer id, String nombre, String pais) {
        Id = id;
        this.nombre = nombre;
        this.pais = pais;
    }


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

}
