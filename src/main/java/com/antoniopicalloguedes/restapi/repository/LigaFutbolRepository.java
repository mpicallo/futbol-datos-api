package com.antoniopicalloguedes.restapi.repository;

import com.antoniopicalloguedes.restapi.model.LigaFutbol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LigaFutbolRepository extends JpaRepository<LigaFutbol,Integer> {

    List<LigaFutbol> findByNombreContaining(String nombre);

    List<LigaFutbol> findByPaisContaining(String pais);


}
