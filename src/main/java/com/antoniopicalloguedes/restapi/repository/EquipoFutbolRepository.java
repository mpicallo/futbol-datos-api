package com.antoniopicalloguedes.restapi.repository;

import com.antoniopicalloguedes.restapi.model.EquipoFutbol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EquipoFutbolRepository extends JpaRepository<EquipoFutbol,Integer> {

    List<EquipoFutbol> findByNombreContaining(String nombre);


}
