package com.antoniopicalloguedes.restapi.repository;

import com.antoniopicalloguedes.restapi.model.EntrenadorFutbol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EntrenadorFutbolRepository extends JpaRepository<EntrenadorFutbol,Integer> {

    List<EntrenadorFutbol> findByNombreContaining(String nombre);

    List<EntrenadorFutbol> findByPaisContaining(String pais);


}
