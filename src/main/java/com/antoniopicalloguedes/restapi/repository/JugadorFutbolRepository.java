package com.antoniopicalloguedes.restapi.repository;

import com.antoniopicalloguedes.restapi.model.JugadorFutbol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JugadorFutbolRepository extends JpaRepository<JugadorFutbol,Integer> {

    List<JugadorFutbol> findByNombreContaining(String nombre);

    List<JugadorFutbol> findByPaisContaining(String pais);


}
